$(document).ready(function () {

    $('select').change(function (e) {
        e.preventDefault()
        const select = $('select').val()
        const type = select.slice(0, 1).toUpperCase() + select.slice(1)
        $("#typeForm").load('/../app/Views/Types/' + type + 'FormView.php');
    })

    $('#massDelete').click(function (e) {
        e.preventDefault()
        let idData = [];
        $('.product-checkbox:checked').each(function () {
            idData.push($(this).attr('id'));
        });
        const ids = idData.join(',');
        if (ids.length <= 0) {
            $('#massDelete-info').html('Please select records.')
        } else {
            $.ajax({
                url: '/products/' + ids,
                type: 'POST',
                _method: 'DELETE',
                data: '_method=DELETE&ids=' + ids,
                dataType: 'text',
                success: function (response) {
                    window.location = '/products/list';
                }
            })
        }
    });

    $('#addProduct').click(function (e) {
        e.preventDefault()
        const valid = validateInput();
        if (valid) {
            const data = $("#addProductForm").serialize();

            $.ajax({
                url: '/products/list',
                type: 'POST',
                data: data,
                dataType: 'text',
                success: function (response) {
                    window.location = '/products/list';
                }
            });
        }
    });

    function validateInput() {
        $('.info').html('');
        let valid = true;
        let price = $('#price').val();

        if ($('#sku').val() === '') {
            $('#sku-info').html('Please submit required data.')
            valid = false;
        }
        if ($('#name').val() === '') {
            $('#name-info').html('Please submit required data.')
            valid = false;
        }
        if (price === '') {
            $('#price-info').html('Please submit required data.')
            valid = false;
        }
        if (isNaN(price)) {
            $('#price-info').html('Please provide data of indicated type.')
            valid = false;
        }
        if ($('#type').val() === '') {
            $('#type-info').html('Please provide product type.');
            valid = false;
        }
        $('#typeForm :input').each(function () {
            let value = $(this).val();
            if (value === '') {
                $('#' + $(this).attr('name') + '-info').html('Please submit required data.');
                valid = false;
            }
            if (isNaN(value)) {
                $('#' + $(this).attr('name') + '-info').html('Please provide data of indicated type.');
                valid = false;
            }
        })
        return valid;
    }
})