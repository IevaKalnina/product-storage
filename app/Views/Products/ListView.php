<?php require_once 'app/Views/Components/Header.php'; ?>
<h2>Product List</h2>
<div class="buttons">
    <form method="GET" action="/product/add">
        <button type="submit">ADD</button>
    </form>
    <div id="delete-button">
        <span class="info" id="massDelete-info"></span>
        <button id="massDelete" type="submit">MASS DELETE</button>
    </div>
</div>
</div>
</header>
<div class="container">
    <div id="products-list" class="list">
        <?php if ($products) : ?>
            <?php foreach ($products as $product) : ?>
                <div class="product-item" id="<?php echo $product->id(); ?>">
                    <input type="checkbox" class="product-checkbox" id="<?php echo $product->id(); ?>">
                    <div style="text-align: center">
                        <span><?php echo $product->sku(); ?></span><br/>
                        <span><?php echo $product->name(); ?></span><br/>
                        <span><?php echo PriceFormatService::unitsToDollars($product->price()); ?></span><br/>
                        <span><?php echo $product->description(); ?></span>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <h4>No products yet.</h4>
        <?php endif; ?>
    </div>
</div>
<?php require_once 'app/Views/Components/Footer.php'; ?>
