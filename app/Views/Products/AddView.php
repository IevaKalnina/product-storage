<?php require_once 'app/Views/Components/Header.php'; ?>
    <h2>Product Add</h2>
    <div class="buttons">
        <div>
            <button id="addProduct" type="submit">Save</button>
        </div>
        <form style="margin-bottom: unset" action="/products/list" method="GET">
            <button type="submit">Cancel</button>
        </form>
    </div>
    </div>
    </header>
    <div class="container">
        <form id="addProductForm">
            <div class="input">
                <div class="input-field">
                    <label for="sku">SKU</label>
                    <input type="text" id="sku" name="sku" maxlength="255">
                </div>
                <span class="info" id="sku-info"></span>
            </div>
            <div class="input">
                <div class="input-field">
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" maxlength="255">
                </div>
                <span class="info" id="name-info"></span>
            </div>
            <div class="input">
                <div class="input-field">
                    <label for="price">Price ($)</label>
                    <input type="text" id="price" name="price" maxlength="11">
                </div>
                <span class="info" id="price-info"></span>
            </div>
            <div class="input">
                <div class="input-field">
                    <label for="type">Type switcher</label>
                    <select name="type" id="type">
                        <option value="">Choose type</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>
                        <option value="dvd">DVD</option>
                    </select>
                </div>
                <span class="info" id="type-info"></span>
            </div>
            <div id="typeForm"></div>

        </form>
    </div>
<?php require_once 'app/Views/Components/Footer.php'; ?>