<div class="input">
    <div class="input-field">
        <label for="height">Height (CM)</label>
        <input type="text" id="height" name="height" maxlength="11">
    </div>
    <span class="info" id="height-info"></span>
</div>
<div class="input">
    <div class="input-field">
        <label for="width">Width (CM)</label>
        <input type="text" id="width" name="width" maxlength="11">
    </div>
    <span class="info" id="width-info"></span>
</div>
<div class="input">
    <div class="input-field">
        <label for="length">Length (CM)</label>
        <input type="text" id="length" name="length" maxlength="11">
    </div>
    <span class="info" id="length-info"></span>
</div>
<p class="description">Please provide dimensions in HxWxL format</p>
