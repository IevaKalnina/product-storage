<div class="input">
    <div class="input-field">
        <label for="weight">Weight (KG)</label>
        <input type="text" id="weight" name="weight" maxlength="11">
    </div>
    <span class="info" id="weight-info"></span>
</div>
<p class="description">Please provide weight</p>