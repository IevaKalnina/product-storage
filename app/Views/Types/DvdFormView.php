<div class="input">
    <div class="input-field">
        <label for="size">Size (MB)</label>
        <input type="text" id="size" name="size" maxlength="11">
    </div>
    <span class="info" id="size-info"></span>
</div>
<p class="description">Please provide size</p>
