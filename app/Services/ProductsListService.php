<?php

namespace App\Services;

use App\Models\Product;
use App\Repositories\ProductsRepository;

class ProductsListService
{
    public function execute(): array
    {
        $products = [];

        $queryBuilder = (new ProductsRepository)->getAll();

        foreach ($queryBuilder as $product) {
            $description = (new DescriptionService())->execute($product['type'], $product['id']);
            $products[] = new Product(
                $product['id'],
                $product['sku'],
                $product['name'],
                $product['price'],
                $product['type'],
                $description
            );
        }
        return $products;
    }
}