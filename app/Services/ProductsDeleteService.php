<?php

namespace App\Services;

use App\Repositories\ProductsRepository;

class ProductsDeleteService
{
    public function execute()
    {
        $ids = explode(',', $_POST['ids']);

        foreach ($ids as $id) {
            $model = ucfirst((new ProductsRepository())->getById($id)['type']);

            $repository = 'App\Repositories\\' . $model . 'Repository';

            (new $repository)->delete($id);
        }

        (new ProductsRepository())->massDelete($ids);
    }
}