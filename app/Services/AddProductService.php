<?php

namespace App\Services;

use App\Repositories\ProductsRepository;
use Ramsey\Uuid\Uuid;

class AddProductService
{
    public function execute()
    {
        $uuid = Uuid::uuid4();

        $product = [
            'id' => $uuid->toString(),
            'sku' => $_POST['sku'],
            'name' => $_POST['name'],
            'price' => ($_POST['price'] * 100),
            'type' => $_POST['type']
        ];

        $model = ucfirst($product['type']);

        $typeService = 'App\Services\TypeServices\Add' . $typeModel . 'Service';

        (new $typeService())->execute($product->id());

        (new ProductsRepository())->store($product);
    }
}