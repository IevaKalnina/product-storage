<?php

namespace App\Services;

class DescriptionService
{
    public function execute(string $type, string $productId): string
    {
        $model = ucfirst($type);

        $repository = 'App\Repositories\\' . $model . 'Repository';

        $typeData = (new $repository())->getByProductId($productId);

        $model = 'App\Models\\' . $model;

        return (new $model($typeData))->description();
    }
}