<?php

namespace App\Services\TypeServices;

use App\Models\Furniture;
use App\Repositories\FurnitureRepository;

class AddFurnitureService
{
    public function execute(string $productId)
    {
        $furnitureDescription = new Furniture([
            'product_id' => $productId,
            'height' => $_POST['height'],
            'width' => $_POST['width'],
            'length' => $_POST['length'],
    ]);

        (new FurnitureRepository())->store($furnitureDescription);
    }
}