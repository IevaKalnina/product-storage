<?php

namespace App\Services\TypeServices;

use App\Models\Book;
use App\Repositories\BookRepository;

class AddBookService
{
    public function execute(string $productId)
    {
        $bookDescription = new Book([
            'product_id' => $productId,
            'weight' => $_POST['weight']
    ]);

        (new BookRepository())->store($bookDescription);
    }
}