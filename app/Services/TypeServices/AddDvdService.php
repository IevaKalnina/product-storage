<?php

namespace App\Services\TypeServices;

use App\Models\Dvd;
use App\Repositories\DvdRepository;

class AddDvdService
{
    public function execute(string $productId)
    {
        $dvdDescription = new Dvd([
            'product_id' => $productId,
            'size' => $_POST['size']
    ]);

        (new DvdRepository())->store($dvdDescription);
    }
}