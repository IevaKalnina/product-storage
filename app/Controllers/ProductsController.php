<?php

namespace App\Controllers;

use App\Services\AddProductService;
use App\Services\ProductsDeleteService;
use App\Services\ProductsListService;

class ProductsController
{
    public function index()
    {
        $products = (new ProductsListService)->execute();

        return require_once __DIR__ . '/../Views/Products/ListView.php';
    }

    public function add()
    {
        return require_once __DIR__ . '/../Views/Products/AddView.php';
    }

    public function store()
    {
        (new AddProductService())->execute();

        header('Location: /products/list');
    }

    public function delete()
    {
        (new ProductsDeleteService())->execute();

        header('Location: /products/list');
    }
}