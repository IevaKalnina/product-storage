<?php

namespace App\Repositories;

use App\Database\DatabaseManager;
use Doctrine\DBAL\Query\QueryBuilder;

class BookRepository
{
    private QueryBuilder $queryBuilder;

    public function __construct()
    {
        $this->queryBuilder = (new DatabaseManager())->query();
    }

    public function getByProductId(string $productId): array
    {
        return $this->queryBuilder
            ->select('*')
            ->from('book')
            ->where('product_id = :productId')
            ->setParameter('productId', $productId)
            ->execute()
            ->fetchAssociative();
    }

    public function delete(string $productId)
    {
        $this->queryBuilder
            ->delete('book')
            ->where('product_id = :productId')
            ->setParameter('productId', $productId)
            ->execute();
    }

    public function store(Book $book)
    {
        $this->queryBuilder
            ->insert('book')
            ->values([
                'product_id' => ':productId',
                'weight' => ':weight'
            ])->setParameters([
                'productId' => $book->productId(),
                'weight' => $book->weight()
            ])->execute();
    }
}