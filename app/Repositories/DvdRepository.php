<?php

namespace App\Repositories;

use App\Database\DatabaseManager;
use Doctrine\DBAL\Query\QueryBuilder;

class DvdRepository
{
    private QueryBuilder $queryBuilder;

    public function __construct()
    {
        $this->queryBuilder = (new DatabaseManager())->query();
    }

    public function getByProductId(string $productId): array
    {
        return $description = $this->queryBuilder
            ->select('*')
            ->from('dvd')
            ->where('product_id = :productId')
            ->setParameter('productId', $productId)
            ->execute()
            ->fetchAssociative();
    }

    public function delete(string $productId)
    {
        $this->queryBuilder
            ->delete('dvd')
            ->where('product_id = :productId')
            ->setParameter('productId', $productId)
            ->execute();
    }

    public function store(Dvd $dvd)
    {
        $this->queryBuilder
            ->insert('dvd')
            ->values([
                'product_id' => ':productId',
                'size' => ':size'
            ])->setParameters([
                'productId' => $dvd->productId(),
                'size' => $dvd->size()
            ])->execute();
    }
}