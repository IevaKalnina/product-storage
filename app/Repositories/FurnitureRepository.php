<?php

namespace App\Repositories;

use App\Database\DatabaseManager;
use Doctrine\DBAL\Query\QueryBuilder;

class FurnitureRepository
{
    private QueryBuilder $queryBuilder;

    public function __construct()
    {
        $this->queryBuilder = (new DatabaseManager())->query();
    }

    public function getByProductId(string $productId): array
    {
        return $this->queryBuilder
            ->select('*')
            ->from('furniture')
            ->where('product_id = :productId')
            ->setParameter('productId', $productId)
            ->execute()
            ->fetchAssociative();
    }

    public function delete(string $productId)
    {
        $this->queryBuilder
            ->delete('furniture')
            ->where('product_id = :productId')
            ->setParameter('productId', $productId)
            ->execute();
    }

    public function store(Furniture $furniture)
    {
        $this->queryBuilder
            ->insert('furniture')
            ->values([
                'product_id' => ':productId',
                'height' => ':height',
                'width' => ':width',
                'length' => ':length'
            ])->setParameters([
                'productId' => $furniture->productId(),
                'height' => $furniture->height(),
                'width' => $furniture->width(),
                'length' => $furniture->length()
            ])->execute();
    }
}