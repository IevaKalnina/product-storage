<?php

namespace App\Repositories;

use App\Database\DatabaseManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;

class ProductsRepository
{
    private QueryBuilder $queryBuilder;

    public function __construct()
    {
        $this->queryBuilder = (new DatabaseManager())->query();
    }

    public function getAll(): array
    {
        return $this->queryBuilder
            ->select('*')
            ->from('products')
            ->orderBy('created_at', 'desc')
            ->execute()
            ->fetchAllAssociative();
    }

    public function getById(string $id): array
    {
        return $this->queryBuilder
            ->select('*')
            ->from('products')
            ->where('id = :id')
            ->setParameter('id', $id)
            ->execute()
            ->fetchAssociative();
    }

    public function massDelete(array $ids)
    {
        $sql = 'DELETE FROM products WHERE id IN (?)';

        (new DatabaseManager())
            ->database()
            ->executeQuery($sql,
                [$ids],
                [Connection::PARAM_STR_ARRAY]
            );
    }

    public function store(Product $product)
    {
        $this->queryBuilder
            ->insert('products')
            ->values([
                'id' => ':id',
                'sku' => ':sku',
                'name' => ':name',
                'price' => ':price',
                'type' => ':type'
            ])->setParameters([
                'id' => $product->id(),
                'sku' => $product->sku(),
                'name' => $product->name(),
                'price' => $product->price(),
                'type' => $product->type(),
            ])->execute();
    }
}