<?php

namespace App\Models;

class Book
{
    private string $productId;
    private float $weight;

    public function __construct(array $data)
    {
        $this->productId = $data['product_id'];
        $this->weight = $data['weight'];
    }
    
    public function productId(): string
    {
        return $this->productId;
    }

    public function weight(): string
    {
        return $this->weight;
    }

    public function description(): string
    {
        return 'Weight: ' . $this->weight . ' KG';
    }
}