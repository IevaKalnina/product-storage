<?php

namespace App\Models;

class Product
{
    private string $sku;
    private string $name;
    private string $type;
    private string $id;
    private int $price;
    private string $description;

    public function __construct(
        string $id,
        string $sku,
        string $name,
        int $price,
        string $type,
        string $description = '')
    {
        $this->id = $id;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
        $this->description = $description;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function sku(): string
    {
        return $this->sku;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function price(): string
    {
        return $this->price;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function description(): string
    {
        return $this->description;
    }
}