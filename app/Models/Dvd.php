<?php

namespace App\Models;

class Dvd
{
    private string $productId;
    private float $size;

    public function __construct(array $data)
    {
        $this->productId = $data['product_id'];
        $this->size = $data['size'];
    }
    
    public function productId(): string
    {
        return $this->productId;
    }

    public function size(): string
    {
        return $this->size;
    }

    public function description(): string
    {
        return 'Size: ' . $this->size . ' MB';

    }

}