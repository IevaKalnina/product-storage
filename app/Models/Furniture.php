<?php

namespace App\Models;

class Furniture
{
    private string $productId;
    private float $height;
    private float $width;
    private float $length;

    public function __construct(array $data)
    {
        $this->productId = $data['product_id'];
        $this->height = $data['height'];
        $this->width = $data['width'];
        $this->length = $data['length'];
    }
    
    public function productId(): string
    {
        return $this->productId;
    }

    public function height(): string
    {
        return $this->height;
    }

    public function length(): string
    {
        return $this->length;
    }

    public function width(): string
    {
        return $this->width;
    }

    public function description(): string
    {
        $dimensions = $this->height . 'x' . $this->width . 'x' . $this->length;
        return 'Dimensions: ' . $dimensions;
    }

}