create table book
(
    id         int auto_increment
        primary key,
    product_id varchar(255) not null,
    weight     float        not null
);

create table dvd
(
    id         int auto_increment
        primary key,
    product_id varchar(255) not null,
    size       int          not null
);

create table furniture
(
    id         int auto_increment
        primary key,
    product_id varchar(255) not null,
    height     float        not null,
    width      float        not null,
    length     float        not null
);

create table products
(
    id         varchar(255)                        not null,
    sku        varchar(255)                        not null,
    name       varchar(255)                        not null,
    price      int                                 not null,
    type       varchar(255)                        not null,
    created_at timestamp default CURRENT_TIMESTAMP null,
    constraint products_id_uindex
        unique (id)
);

alter table products
    add primary key (id);

