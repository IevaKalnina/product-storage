# Product Storage

![Alt Text](gif/ProductStorage.gif)

### Dev setup

```
composer install
copy .env.example to .env
fill in .env with your configuration values
create new database
import sql/db.sql into your database
```
